package sample.panels;

/*
класс, опиывающий панель для добавления (удаления) новой прямой дороги
 */

import sample.OnAddNoteListener;
import sample.UndirectedEdge;
import sample.interfaces.Constants;
import sample.interfaces.Deletable;
import sample.interfaces.MessageOutputable;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.ImageIcon;

import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RoadInput extends JPanel implements ActionListener {

    private int position;
    private boolean isFreezed = false;

    private JLabel l_position;
    private JLabel l_first;
    private JTextField tf_first;
    private JLabel l_second;
    private JTextField tf_second;
    private JButton b_confirm;

    private JComponent parent;
    private MessageOutputable message_outputable;
    private Deletable deletable;
    private OnAddNoteListener listener;

    public RoadInput(JComponent parent, int position){
        this.parent = parent;
        //если тип объекта parent соответствует типу MessageOutputable
        if (parent instanceof  MessageOutputable)
            message_outputable = (MessageOutputable) parent;
        //если тип объекта parent соответствует типу Deletable
        if (parent instanceof Deletable)
            deletable = (Deletable) parent;
        //если тип объекта parent соответствует типу OnAddNoteListener
        if (parent instanceof OnAddNoteListener)
            listener = (OnAddNoteListener) parent;
        this.position = position;
        //установка мененджера компановки FlowLayout с горизонтальным зазором в 15 пикселей
        setLayout(new FlowLayout(FlowLayout.CENTER, 15,0));

        //инициализация элементов панели
        //создание метки с номером позиции данного ребра
        l_position = new JLabel(Integer.toString(position+1)+".");
        l_first = new JLabel("От:");
        tf_first = new JTextField(3);
        l_second = new JLabel("к:");
        tf_second = new JTextField(3);
        //создание кнопки с иконкой add.png
        b_confirm = new JButton(new ImageIcon(Constants.icons_folder+"add.png"));
        //добавление к кнопке обработчика событий
        b_confirm.addActionListener(this);
        //добавление к кнопке всплывающей подсказки
        b_confirm.setToolTipText("Добавить описание дороги");

        //добавление элементов в панель
        add(l_position);
        add(l_first);
        add(tf_first);
        add(l_second);
        add(tf_second);
        add(b_confirm);

        //метод для пересчета макета и определения размеров элементов
        doLayout();
        //задание кнопке предпочтительных размеров со стороной, равной высоте поля для ввода
        b_confirm.setPreferredSize(new Dimension(tf_first.getSize().height, tf_first.getSize().height));
    }

    //установка данных ребра в поля ввода
    public void set_data(UndirectedEdge e){
        tf_first.setText(Integer.toString(e.get_u()));
        tf_second.setText(Integer.toString(e.get_v()));
        freeze();
    }

    //установка новой позиции ребра
    public void set_position(int position){
        this.position = position;
        l_position.setText(Integer.toString(position+1)+".");
    }

    /*"замарозка" панели: отключение возможности редактирования полей ввода,
    смена назначения кнопки с добавления на удаление*/
    public void freeze(){
        tf_first.setEditable(false);
        tf_second.setEditable(false);
        b_confirm.setIcon(new ImageIcon(Constants.icons_folder+"delete.png"));
        b_confirm.setToolTipText("Удалить описание дороги");
        isFreezed = true;
    }

    //обработчик события нажатия на кнопку
    @Override
    public void actionPerformed(ActionEvent e){
        //если назначение кнопки - добавление
        if (!isFreezed) {
            boolean result;
            int u, v;
            //получение значений из полей ввода, их приведение к типу Integer
            try {
                u = Integer.parseInt(tf_first.getText());
                v = Integer.parseInt(tf_second.getText());
                //исполнение метода action родительского компонента и получение результата
                result = listener.action(new UndirectedEdge(u, v));
            } catch (NumberFormatException exc) {
                //обработка ошибки в случае невозможности привдения значений к типу Integer
                result = false;
                message_outputable.output("Неподдерживаемое значение");
            }
            //если метод action родителя завершился успешно
            if (result)
                freeze();
            //иначе, если переданные данные не соответствуют требованиям
            else {
                tf_first.setText("");
                tf_second.setText("");
            }
        }
        //если назаначение кнопки - удаление
        else
            deletable.delete(position);
    }
}
