package sample.panels;

/*
класс, описывающий панель, содержащий текстовое поле для ввода данных
и пояснительный текст
 */

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.event.DocumentListener;

import java.awt.FlowLayout;

public class ParamInput extends JPanel{

    private JLabel l_message;
    private JTextField tf_input;
    private JComponent parent;
    private DocumentListener listener;

    public ParamInput(JComponent parent, String mes){
        this.parent = parent;
        //если тип объекта parent соответствует типу DocumentListener
        if (parent instanceof DocumentListener)
            listener = (DocumentListener) parent;
        //установка менеджера компонента FlowLayout
        setLayout(new FlowLayout(FlowLayout.CENTER));

        //инициализация элементов панели
        //создание метки с текстом mes
        l_message = new JLabel(mes);
        //создание поля для ввода, размером 3 столбца
        tf_input = new JTextField(3);
        //добавление к текстовому полю слушателя событий изменения его содержимого
        tf_input.getDocument().addDocumentListener(listener);

        //добавление элементов в панель
        add(l_message);
        add(tf_input);
        //метод для пересчета макета и определения размеров элементов
        doLayout();
    }

    //установка значения parameter в поле tf_input
    public void set_parameter(int parameter){
        tf_input.setText(Integer.toString(parameter));
    }

    //возврат целого значения, введенного в поле tf_input
    public int get_parameter(){
        int p;
        String p_str = tf_input.getText();
        try{
            p = Integer.parseInt(p_str);
        }catch (NumberFormatException exc){
            return -1;
        }
        return p;
    }
}
