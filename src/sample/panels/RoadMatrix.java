package sample.panels;

import sample.OnAddNoteListener;
import sample.UndirectedEdge;
import sample.interfaces.Deletable;
import sample.interfaces.ParamsGetterable;

import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class RoadMatrix extends JPanel implements Deletable {

    private JComponent parent;
    private ParamsGetterable params_getterable;
    private JToggleButton[][] matrix;
    private JLabel[] numbers;
    private GridBagConstraints c;
    private OnAddNoteListener listener;
    private Deletable deletable;

    RoadMatrix(JComponent parent){
        this.parent = parent;
        //если тип объекта parent соответствует типу ParamsGetterable
        if (parent instanceof ParamsGetterable)
            params_getterable = (ParamsGetterable) parent;
        //если тип объекта parent соответствует типу OnAddNoteListener
        if (parent instanceof OnAddNoteListener)
            listener = (OnAddNoteListener) parent;
        //если тип объекта parent соответствует типу Deletable
        if (parent instanceof Deletable)
            deletable = (Deletable) parent;
        //установка менеджера компоновки
        setLayout(new GridBagLayout());
        /* объект GridBagConstraints, определяющий параметры размещения компонент:
        номер столбца gridx: 0
        номер строки: 0
        количество ячеек, занимаемых одним компонентом по горизонтали gridwidth: 1
        количество ячеек, занимаемых одним компонентом по вертикали gridwidth: 1
        "вес" элементов по горизонтали: 0 - элементы не занимают дополнительного пространства
        "вес" элементов по вертикали: 0 - элементы не занимают дополнительного пространства
        притягивание элементов в ячейке: CENTER - по центру
        растягивание элемента в ячейке: NONE - элементы не растягиваются
        отступы элемента от границ ячейки: Insets - нет отступов
        дополнительные отсупы по горизонтали и вериткали: ipadx, ipady - 0, 0 */
        c =  new GridBagConstraints(0, 0, 1, 1,
                0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0);
    }

    //метод, уведомляющий о том, что значение параметра n изменилось
    public void notify_param_was_changed(){
        int i, j;
        //получение нового параметра n
        int n = params_getterable.get_parameter(ParamsGetterable.N_NUMBER);
        if (n <= 0)
            return;
        //если матрица уже была создана
        if (matrix != null) {
            //удалить все элементы матрицы и метки с номерами строк и столбцов
            for (i = 0; i < matrix.length; i++) {
                for (j = 0; j < matrix[i].length; j++) {
                    remove(matrix[i][j]);
                    remove(numbers[i]);
                    remove(numbers[i+matrix.length]);
                }
            }
        }

        //создание матрицы новой размерности
        matrix = new JToggleButton[n][n];
        //создание массива меток новой размерности с номерами строк и столбцов
        numbers = new JLabel[2*n];
        //принудительный запус очистки мусора
        System.gc();
        for (i=0; i<n; i++){
            c.gridy = i+1;
            for (j=0; j<n; j++){
                c.gridx = j+1;
                //создание новой кнопки-переключателя
                matrix[i][j] = new JToggleButton();
                //установка предпочтительных размеров в 20*20 пикселей
                matrix[i][j].setPreferredSize(new Dimension(20, 20));
                final int x = i, y = j;
                //добавление к кнопке слушателя событи
                matrix[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        //если кнопка нажата
                        if (matrix[x][y].isSelected()){
                            //исполнение метода action родителя
                            listener.action(new UndirectedEdge(x+1, y+1));
                            //установка симметричной относительно главной диагонали матрицы кнопки
                            matrix[y][x].setSelected(true);
                        }
                        else{
                            //исполнение метода deletable родителя
                            deletable.delete(new UndirectedEdge(x+1, y+1));
                            //сброс симметричной относительно главной диагонали матрицы кнопки
                            matrix[y][x].setSelected(false);
                        }
                    }
                });
                //добавление элемента матрицы в контейнер
                add(matrix[i][j], c);
            }
        }
        //создание и добавление массива меток с номерами строк и столбцов
        for (i=0; i<n; i++){
            //значения от 0..n-1 - номера строк
            numbers[i] = new JLabel(Integer.toString(i+1));
            //значения от n до 2n-1 - номера столбцов
            numbers[i+n] = new JLabel(Integer.toString(i+1));
            c.gridx = 0;
            c.gridy = i+1;
            add(numbers[i], c);
            c.gridy = 0;
            c.gridx = i+1;
            add(numbers[i+n], c);
        }
        //принудительное обновление дерева компонентов
        SwingUtilities.updateComponentTreeUI(this);
    }

    //установка уже существующего списка ребер
    public void set_data(ArrayList<UndirectedEdge> data){
        int i, j;
        //очистка имеющейся матрицы
        if (matrix != null) {
            for (i = 0; i < matrix.length; i++) {
                for (j = 0; j < matrix[i].length; j++) {
                    matrix[i][j].setSelected(false);
                }
            }
            //установка симметричных относительно главной диагонали кнопок
            for (UndirectedEdge e: data){
                matrix[e.get_u()-1][e.get_v()-1].setSelected(true);
                matrix[e.get_v()-1][e.get_u()-1].setSelected(true);
            }
        }
    }

    //метод для сброса кнопок по значению ребра
    @Override
    public void delete(UndirectedEdge e){
        matrix[e.get_u()-1][e.get_v()-1].setSelected(false);
        matrix[e.get_v()-1][e.get_u()-1].setSelected(false);
        deletable.delete(e);
    }

    //перегруженный метод из интерфейса Deletable
    @Override
    public void delete(int position){
        deletable.delete(position);
    }
}
