package sample;

/*
класс, опиываюющий панель, содержащую все необходимые компоненты для практической работы №2
 */

import sample.interfaces.Constants;
import sample.interfaces.MessageOutputable;
import sample.interfaces.ParamsGetterable;
import sample.panels.ParamInput;
import sample.panels.RoadGraph;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Scanner;

public class DMPR2Component extends JPanel implements MessageOutputable, ParamsGetterable, DocumentListener {

    private ParamInput input_n, input_m;
    private JButton b_load;
    private JLabel l_error;
    private JLabel l_res;
    private JLabel l_list;
    private RoadGraph road_graph;
    private JButton b_confirm;
    private JButton b_save;
    private GridBagConstraints c;

    DMPR2Component(){
        //установка менеджера компановки: размещение в ячейках таблицы с возможностями настройки
        setLayout(new GridBagLayout());
        /* объект GridBagConstraints, определяющий параметры размещения компонент:
        номер столбца gridx: 0
        номер строки: 0
        количество ячеек, занимаемых одним компонентом по горизонтали gridwidth: 2
        количество ячеек, занимаемых одним компонентом по вертикали gridwidth: 1
        "вес" элементов по горизонтали: 1 - элементы занимают все свободное пространство
        "вес" элементов по вертикали: 0 - элементы не занимают дополнительного пространства
        притягивание элементов в ячейке WEST - левый край ячейки
        растягивание элемента в ячейке: HORIZONTAL - элементы растягиваются по горизонтали
        отступы элемента от границ ячейки: Insets - по 5 пикселей сверху и снизу, по 15 слева и справа
        дополнительные отсупы по горизонтали и вериткали: ipadx, ipady - 0, 0 */
        c =  new GridBagConstraints(0, 0, 2, 1,
                1, 0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
                new Insets(5, 15, 5, 15), 0, 0);

        //инициализация элементов панели
        //объект типа ParamInput: поле для ввода числа N
        input_n = new ParamInput(this, "Введите количество городов N: ");
        //конпка для загрузки данных с иконкой load.png
        b_load = new JButton(new ImageIcon(Constants.icons_folder+"load.png"));
        //добавление к кнопке всплывающей подсказки
        b_load.setToolTipText("Загрузить данные из файла");
        //добавление к кнопке слушателя события нажатия
        b_load.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                load_data();
            }
        });

        l_list = new JLabel("Список прямых дорог:");
        //создание объекта типа RoadGraph
        road_graph = new RoadGraph(this);
        //объект типа ParamInput: поле для ввода числа M
        input_m = new ParamInput(this, "Введите число M: ");
        //кнопка для рассчета результата
        b_confirm = new JButton("Рассчитать");
        b_confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                execute();
            }
        });
        //конпка для сохранения данных с иконкой load.png
        b_save = new JButton(new ImageIcon(Constants.icons_folder+"save.png"));
        b_save.setToolTipText("Сохранить данные в файл");
        b_save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save_data();
            }
        });

        //метка для вывода результата
        l_res = new JLabel("Результат: ");
        //метка для вывода сообщений об ошибке
        l_error = new JLabel("");
        //установке красного цвета шрифта в метке для вывода ошибок
        l_error.setForeground(Color.RED);

        //добавление элементов в панель с необходимыми параметрами
        c.gridwidth = 1;
        add(input_n, c);
        c.gridx = 1;
        add(b_load, c);

        c.gridy = 1;
        c.gridx = 0;
        c.gridwidth = 2;
        add(l_list, c);

        c.gridy = 2;
        //заполнение всего свободного пространства
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 1;
        add(road_graph, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 0;
        c.gridy = 3;
        add(input_m, c);

        c.gridy = 4;
        c.gridwidth = 1;
        add(b_confirm, c);
        c.gridx = 1;
        add(b_save, c);

        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 5;
        add(l_res, c);

        c.gridy = 6;
        c.fill = GridBagConstraints.NONE;
        //выравнивание по центру ячейки
        c.anchor = GridBagConstraints.CENTER;
        add(l_error, c);

        //метод для пересчета макета и определения размеров элементов
        doLayout();
        //задание кнопкам предпочтительных размеров со стороной, равной высоте кнопки b_confirm
        b_load.setPreferredSize(new Dimension(b_confirm.getSize().height, b_confirm.getSize().height));
        b_save.setPreferredSize(new Dimension(b_confirm.getSize().height, b_confirm.getSize().height));
    }

    //метод для рассчета результата
    private void execute(){
        int i, n, m, num;
        ArrayList<UndirectedEdge> list_of_edges;
        int[] array_of_vertex;
        String answer = "Результат: ";
        //получение параметра n
        n = get_parameter(ParamsGetterable.N_NUMBER);
        //если значение не соответствует требованиям, возврат из метода
        if (n <= 0)
            return;
        //получение параметра m
        m = get_parameter(ParamsGetterable.M_NUMBER);
        //если значение не соответствует требованиям, возврат из метода
        if (m <= 0)
            return;
        //получение списка ребер из объекта road_list
        list_of_edges = road_graph.get_list_of_edges();
        //если не введено ни одного ребра
        if (list_of_edges.size() == 0){
            answer = answer.concat("-");
            l_res.setText(answer);
            return;
        }

        //массив, содержащий в i-ом элементе количество городов, соединенных с i-ым прямой дорогой
        array_of_vertex = new int[n];
        for (UndirectedEdge e: list_of_edges){
            //если ребро не является петлей
            if (!e.isLoop()) {
                array_of_vertex[e.get_u() - 1]++;
                array_of_vertex[e.get_v() - 1]++;
            }
        }
        //количество городов, соответствующих требованиям
        num = 0;
        //формирование строки результата
        for (i=0; i<n; i++){
            //если количество городов, соединенных прямыми дорогами с i-ым больше m
            if (array_of_vertex[i] > m){
                //присоединение к концу строки результата
                answer = answer.concat(Integer.toString(i+1)+"; ");
                num++;
            }
        }
        //если не найдено ни одного подходящего города
        if (num == 0)
            answer = answer.concat("-");
        //вывод результата на экран
        l_res.setText(answer);
    }

    //метод для сохранения введенных данных в файл
    private void save_data(){
        int n, m;
        //получение всех введенных данных, проверка их при этом на соответствие требованиям
        ArrayList<UndirectedEdge> list_of_edges;
        n = get_parameter(ParamsGetterable.N_NUMBER);
        if (n <= 0)
            return;
        m = get_parameter(ParamsGetterable.M_NUMBER);
        if (m <= 0)
            return;
        list_of_edges = road_graph.get_list_of_edges();

        //создание нового объекта типа JFileChooser: диалог выбора файлов
        JFileChooser file_chooser = new JFileChooser();
        //установка заголовка окна объекта file_chooser
        file_chooser.setDialogTitle("Сохранение файла");
        //установка режима работы file_chooser: выбор только файлов
        file_chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        //установка фильтра файлов по расширению
        FileNameExtensionFilter filter
                = new FileNameExtensionFilter("."+Constants.extension, Constants.extension);
        file_chooser.setFileFilter(filter);
        //получение результата выбора пользователя при закрытии file_chooser
        int result = file_chooser.showSaveDialog(this);
        //если выбор файла прошел успешно
        if (result == JFileChooser.APPROVE_OPTION) {
            //получение выбранного файла
            File file = file_chooser.getSelectedFile();
            //получение выбранного пользователем расширения
            String extension = file_chooser.getFileFilter().getDescription();
            //если расширение соответствует .dmpr2
            if (extension.equals("."+Constants.extension))
                //создать новый файл по прежнему пути с добавлением расширения
                file = new File(file.getAbsolutePath()+"."+Constants.extension);
            //запись данных в файл
            try {
                //открытие потока вывода
                BufferedWriter bw = new BufferedWriter(new FileWriter(file));
                //запись параметров n m в первой строке
                bw.write(Integer.toString(n)+" "+Integer.toString(m)+"\n");
                //запись двух концов каждого ребра через пробел на всех последующих строках
                for (UndirectedEdge e:list_of_edges){
                    bw.write(Integer.toString(e.get_u())+" "+Integer.toString(e.get_v())+"\n");
                }
                //очистка буфера
                bw.flush();
                //закрытие потока вывода
                bw.close();
            }catch (IOException exc){
                //обработка возможных ошибок записи в файл
                output("Ошибка при сохранении");
                return;
            }
            //вывод информационного сообщения о том, что файл успешно сохранен
            JOptionPane.showMessageDialog(this,
                    "Файл '" + file +
                            "' сохранен");
        }
    }

    //метод для загрузки введенных данных из файла
    private void load_data(){
        int n, m, u, v;
        ArrayList<UndirectedEdge> list_of_edges = new ArrayList<>();
        //создание нового объекта типа JFileChooser: диалог выбора файлов
        JFileChooser file_chooser = new JFileChooser();
        //установка заголовка окна объекта file_chooser
        file_chooser.setDialogTitle("Выбор директории");
        //установка режима работы file_chooser: выбор только файлов
        file_chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        //установка фильтра файлов по расширению
        FileNameExtensionFilter filter
                = new FileNameExtensionFilter("."+Constants.extension, Constants.extension);
        file_chooser.setFileFilter(filter);
        //получение результата выбора пользователя при закрытии file_chooser
        int result = file_chooser.showOpenDialog(this);
        //если выбор файла прошел успешно
        if (result == JFileChooser.APPROVE_OPTION ) {
            //получение выбранного файла
            File file = file_chooser.getSelectedFile();
            try{
                //создание нового объекта типа Scanner на основе потока FileReader
                Scanner scanner = new Scanner(new FileReader(file));
                //считывание параметров n и m из первой строки
                n = scanner.nextInt();
                m = scanner.nextInt();
                //пока в файле еще имеются непрочитанные значения
                while (scanner.hasNext()){
                    u = scanner.nextInt();
                    v = scanner.nextInt();
                    //считывание и добавление в список очередного ребра
                    list_of_edges.add(new UndirectedEdge(u, v));
                }
                //передача считанного списка ребер в объект road_graph
                road_graph.set_data(list_of_edges);
                input_n.set_parameter(n);
                input_m.set_parameter(m);
            }catch (IOException exc){
                //обработка возможных ошибок чтения из файла
                output("Ошибка при загрузке");
                return;
            }
            //вывод информационного сообщения о том, что файл успешно открыт
            JOptionPane.showMessageDialog(this,
                    "Файл '" + file +
                            "' открыт");
        }
    }

    //метод интерфейса MessageOutputable: передача переданной строки в метку l_error
    @Override
    public void output(String message){
        l_error.setText(message);
    }

    //метод интерфейса ParamsGetterable: возврат значения n или m, проверка его при этом на соответсвие требованиям
    @Override
    public int get_parameter(int num){
        int p = -1;
        switch (num){
            case ParamsGetterable.N_NUMBER:
                p = input_n.get_parameter();
                break;
            case ParamsGetterable.M_NUMBER:
                p = input_m.get_parameter();
                break;
        }
        //елси полученное значение соответствует требованиям
        if (p > 0) {
            output("");
            return p;
        }
        else {
            output("Несоответсвие диапазону допустимых значений");
            return -1;
        }
    }

    //обработчик события добавления текста в текстовое поле
    @Override
    public void insertUpdate(DocumentEvent e){
        //Уведомить объект road_graph о том, что значение параметра изменилось
        road_graph.notify_param_was_changed();
    }

    //обработчик события удаления текста в текстовом поле
    @Override
    public void removeUpdate(DocumentEvent e){
        road_graph.notify_param_was_changed();
    }

    //обработчик события изменения текста в текстовом поле
    @Override
    public void changedUpdate(DocumentEvent e){
        road_graph.notify_param_was_changed();
    }
}