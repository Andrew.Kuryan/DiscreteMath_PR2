package sample.panels;

import sample.OnAddNoteListener;
import sample.UndirectedEdge;
import sample.interfaces.Deletable;
import sample.interfaces.MessageOutputable;
import sample.interfaces.ParamsGetterable;

import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.*;
import java.util.ArrayList;

public class RoadGraph extends JPanel implements MessageOutputable, ParamsGetterable, OnAddNoteListener, Deletable {

    private ArrayList<UndirectedEdge> list_of_edges = new ArrayList<>();
    private JComponent parent;
    private JTabbedPane jtp;
    private RoadList road_list;
    private RoadMatrix road_matrix;
    private MessageOutputable message_outputable;
    private ParamsGetterable params_getterable;

    public RoadGraph(JComponent parent){
        this.parent = parent;
        //если тип объекта parent соответствует типу MessageOutputable
        if (parent instanceof MessageOutputable)
            message_outputable = (MessageOutputable) parent;
        //если тип объекта parent соответствует типу ParamsGetterable
        if (parent instanceof ParamsGetterable)
            params_getterable = (ParamsGetterable) parent;
        //установка менеджера компоновки
        setLayout(new GridBagLayout());
        /* объект GridBagConstraints, определяющий параметры размещения компонент:
        номер столбца gridx: 0
        номер строки: 0
        количество ячеек, занимаемых одним компонентом по горизонтали gridwidth: 1
        количество ячеек, занимаемых одним компонентом по вертикали gridwidth: 1
        "вес" элементов по горизонтали: 1 - элементы занимают все свободное пространство
        "вес" элементов по вертикали: 1 - элементы занимают дополнительного пространства
        притягивание элементов в ячейке: CENTER - по центру
        растягивание элемента в ячейке: BOTH - элементы растягиваются во всех направлениях
        отступы элемента от границ ячейки: Insets - нет отступов
        дополнительные отсупы по горизонтали и вериткали: ipadx, ipady - 0, 0 */
        GridBagConstraints c =  new GridBagConstraints(0, 0, 1, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0);
        //создание нового объекта панели с вкладками
        jtp = new JTabbedPane();
        road_list = new RoadList(this);
        //создание нового объекта типа JScrollPane - поля с полосами прокрутки
        final JScrollPane jsp_list = new JScrollPane(road_list);
        //политика отображения горизонтальных полос прокрутки: никогда
        jsp_list.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        //политика отображения горизонтальных полос прокрутки: всегда
        jsp_list.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        //добавление новой вкладки с названием "Список" и объектом jsp_list
        jtp.addTab("Список", jsp_list);

        road_matrix = new RoadMatrix(this);
        final JScrollPane jsp_matrix = new JScrollPane(road_matrix);
        //политика отображения горизонтальных полос прокрутки: при надобности
        jsp_matrix.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        //политика отображения горизонтальных полос прокрутки: надобности
        jsp_matrix.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        //добавление новой вкладки с названием "М-ца смежности" и объектом jsp_matrix
        jtp.addTab("М-ца смежности", jsp_matrix);
        //добавление слушателя события переключения вкладок
        jtp.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                //если пользователь переключился на вкладку списка
                if (jtp.getSelectedComponent().equals(jsp_list))
                    road_list.set_data(list_of_edges);
                //если пользователь переключился на вкладку матрицы
                if (jtp.getSelectedComponent().equals(jsp_matrix))
                    road_matrix.set_data(list_of_edges);
            }
        });
        //добавление объекта jtp в компонент
        add(jtp, c);
    }

    //возврат списка ребер
    public ArrayList<UndirectedEdge> get_list_of_edges(){
        return list_of_edges;
    }

    //установка уже существующего списка ребер
    public void set_data(ArrayList<UndirectedEdge> data){
        //очистка существующего спика ребер
        list_of_edges.clear();
        //запуск очистки мусора
        System.gc();
        //копирование данных из массива data в list_of_edges
        list_of_edges.addAll(data);
        //передача массива data в методы set_data панелей двух вкладок
        road_list.set_data(data);
        road_matrix.set_data(data);
    }

    //удаление элемента из списка ребер по его позиции
    @Override
    public void delete(int position){
        list_of_edges.remove(position);
    }

    //удаление элемента из списка ребер по описанию ребра
    @Override
    public void delete(UndirectedEdge e){
        int i, pos = -1;
        //поиск позиции ребра
        for (i=0; i<list_of_edges.size(); i++){
            if (list_of_edges.get(i).equals(e)){
                pos = i;
                break;
            }
        }
        if (pos >= 0)
            list_of_edges.remove(pos);
    }

    //метод, сообщающий объекту road_matrix об изменении параметра
    public void notify_param_was_changed(){
        //список для хранения ссылок на ребра, подлежащие удалению
        ArrayList<UndirectedEdge> edges_to_delete = new ArrayList<>();
        //получение нового параметра n
        int n = params_getterable.get_parameter(ParamsGetterable.N_NUMBER);
        if (n<0)
            return;
        /*добавление в edges_to_delete ребер, максимальное значение концов которых
        больше нового значения n*/
        for (UndirectedEdge e: list_of_edges){
            if (e.get_max_value() > n){
                edges_to_delete.add(e);
            }
        }
        //удаление отмеченных ребер из list_of_edges
        for (UndirectedEdge e: edges_to_delete){
            list_of_edges.remove(e);
        }
        //уведомление road_matrix о том, что значение параметра изменилось
        road_matrix.notify_param_was_changed();
        //передача в объекты представлений графа нового списка ребер
        road_matrix.set_data(list_of_edges);
        road_list.set_data(list_of_edges);
    }

    //метод интерфейса OnAddNoteListener
    @Override
    public boolean action(UndirectedEdge e){
        boolean isExist = false;
        //проверка на существование такого же ребра в списке ребер
        for (UndirectedEdge edge: list_of_edges){
            if (edge.equals(e)){
                isExist = true;
                break;
            }
        }
        //если такое же ребро не было найдено
        if (!isExist){
            //получение числа n из родительского компонента
            int n = params_getterable.get_parameter(ParamsGetterable.N_NUMBER);
            //если значения концов ребра больше нуля и меньше количества городов n
            if (e.get_u() <= n && e.get_v() <= n && e.get_u() > 0 && e.get_v() > 0) {
                list_of_edges.add(e);
                //сброс значения в строке ошибок
                message_outputable.output("");
            }
            //если значения концов ребра не соответствуют допустимому диапазону
            else {
                isExist = true;
                output("Несоответсвие диапазону допустимых значений");
            }
        }
        //если такое же значение уже ранее вводилось
        else{
            message_outputable.output("Данная дорога уже вводилась");
        }
        return !isExist;
    }

    //метод output интерфейса MessageOutputable
    @Override
    public void output(String message){
        message_outputable.output(message);
    }

    //метод get_parameter интерфейса ParamsGetterable
    @Override
    public int get_parameter(int num){
        return params_getterable.get_parameter(num);
    }
}
