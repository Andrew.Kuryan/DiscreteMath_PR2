package sample;

/*
класс, описывающий неориентированное ребро графа
*/

public class UndirectedEdge {

    //номера вершин - концов ребра
    private int u;
    private int v;

    public UndirectedEdge(int u, int v){
        this.u = u;
        this.v = v;
    }

    public int get_u(){
        return u;
    }

    public int get_v(){
        return v;
    }

    //метод, возвращающий большее значение из v и u
    public int get_max_value(){
        if (u > v)
            return u;
        else
            return v;
    }

    //проверка, не является ли данное ребро петлей
    public boolean isLoop(){
        //если начало и конец ребра совпадают
        if (u == v)
            return true;
        else
            return false;
    }

    /*возвращает true, если первый конец первого ребра совпадает с первым второго
    и второй первого со вторым второго или наоборот
    */
    @Override
    public boolean equals(Object obj) {
        if ((((UndirectedEdge) obj).get_u() == u && ((UndirectedEdge) obj).get_v() == v) ||
                (((UndirectedEdge) obj).get_v() == u && ((UndirectedEdge) obj).get_u() == v))
            return true;
        else
            return false;
    }

    @Override
    public String toString(){
        return Integer.toString(u)+" <-> "+Integer.toString(v);
    }
}

