package sample;

/*
адрес расположения исходного кода:
https://gitlab.com/Andrew.Kuryan/DiscreteMath_PR2.git
 */

import sample.interfaces.Constants;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class Main{

    Main(){
        //создание графического окна с заголовком
        JFrame frame = new JFrame("Практическая работа №2");
        //установка размеров окна
        frame.setSize(Constants.xSize, Constants.ySize);
        //установка операции по нажатию на кнопку закрытия
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        DMPR2Component component = new DMPR2Component();
        //добавление в окно объекта DMPR2Component
        frame.add(component);
        //установка видимости графического окна
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //установка оформления, соответствующего системному
        try {
            javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch (Exception exc){
            System.err.println("Ошибка при установке системной темы");
            exc.printStackTrace();
        }
        //создание объекта Main в потоке диспетчеризации событий
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main();
            }
        });
    }
}