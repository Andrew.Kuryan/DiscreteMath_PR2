package sample.panels;

/*
класс, описывающий панель, содержащую список прямых дорог
 */

import sample.*;
import sample.interfaces.Deletable;
import sample.interfaces.MessageOutputable;
import sample.interfaces.ParamsGetterable;

import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import java.util.ArrayList;

public class RoadList extends JPanel implements OnAddNoteListener, MessageOutputable, Deletable {

    private ArrayList<RoadInput> list_of_items = new ArrayList<>();
    private GridBagConstraints c;
    private JComponent parent;
    private MessageOutputable message_outputable;
    private ParamsGetterable params_getterable;
    private OnAddNoteListener listener;
    private Deletable deletable;

    public RoadList(JComponent parent){
        this.parent = parent;
        //если тип объекта parent соответствует типу MessageOutputable
        if (parent instanceof  MessageOutputable)
            message_outputable = (MessageOutputable) parent;
        //если тип объекта parent соответствует типу ParamsGetterable
        if (parent instanceof  ParamsGetterable)
            params_getterable = (ParamsGetterable) parent;
        //если тип объекта parent соответствует типу OnAddNoteListener
        if (parent instanceof OnAddNoteListener)
            listener = (OnAddNoteListener) parent;
        //если тип объекта parent соответствует типу Deletable
        if (parent instanceof Deletable)
            deletable = (Deletable) parent;

        //установка менеджера компановки: размещение в ячейках таблицы с возможностями настройки
        setLayout(new GridBagLayout());
        /* объект GridBagConstraints, определяющий параметры размещения компонент:
        номер столбца gridx: o
        номер строки: RELATIVE - размещение элементов один над другим
        количество ячеек, занимаемых одним компонентом по горизонтали gridwidth: 1
        количество ячеек, занимаемых одним компонентом по вертикали gridwidth: 1
        "вес" элементов по горизонтали: 0 - элементы не занимают дополнительного пространства
        "вес" элементов по вертикали: 0 - элементы не занимают дополнительного пространства
        притягивание элементов в ячейке: NORTHWEST - левый верхний угол
        растягивание элемента в ячейке: NONE - элементы не растягиваются
        отступы элемента от границ ячейки: Insets - по 5 пикселей с каждой стороны
        дополнительные отсупы по горизонтали и вериткали: ipadx, ipady - 0, 0 */
        c =  new GridBagConstraints(0, GridBagConstraints.RELATIVE, 1, 1,
                0, 0,
                GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                new Insets(5, 5, 5, 5), 0, 0);
        //создание нового пустого объекта RoadInput
        RoadInput ri = new RoadInput(this, 0);
        //добавление этого объекта в список элементов list_of_items
        list_of_items.add(ri);
        //добавление объекта ri на панель с параметрами c
        add(ri, c);
    }

    //установка уже существующего списка ребер
    public void set_data(ArrayList<UndirectedEdge> data){
        int i, num_of_roads;
        //колчество уже введенных прямых дорог
        num_of_roads = list_of_items.size()-1;
        //удаление всех значений, введенных до этого
        for (i=0; i<num_of_roads; i++){
            remove(list_of_items.get(0));
            list_of_items.remove(0);
        }
        //перенумерация оставшихся в контейнере элементов
        for (i=0; i<list_of_items.size(); i++) {
            list_of_items.get(i).set_position(i);
        }
        //удаление последнего пустого объекта RoadInput
        remove(list_of_items.get(0));
        list_of_items.remove(0);

        //создание и установка объектов RoadInput
        for (i=0; i<data.size(); i++){
            RoadInput ri = new RoadInput(this, i);
            ri.set_data(data.get(i));
            list_of_items.add(ri);
            add(ri, c);
        }
        //создание пустого объекта RoadInput для возможного дополнительного ввода
        RoadInput ri = new RoadInput(this, data.size());
        list_of_items.add(ri);
        add(ri, c);
        //принудительное обновление дерева компонентов
        SwingUtilities.updateComponentTreeUI(this);
    }

    //удаление элемента из списка
    @Override
    public void delete(int position){
        int i;
        //удаление элемента из компонента
        remove(list_of_items.get(position));
        //удаление элемента из списка элементов
        list_of_items.remove(position);
        //удаление ребра из списка ребер
        deletable.delete(position);
        //перенумерация оставшихся в контейнере элементов
        for (i=0; i<list_of_items.size(); i++) {
            list_of_items.get(i).set_position(i);
        }
        //принудительное обновление дерева компонентов
        SwingUtilities.updateComponentTreeUI(this);
        //принудительный запуск процесса очистки мусора
        System.gc();
    }

    //перегруженный метод из интерфейса Deletable
    @Override
    public void delete(UndirectedEdge e){
        deletable.delete(e);
    }

    //метод интерфейса MessageOutputable, исполняющий метод output родительского компонента
    @Override
    public void output(String message){
        message_outputable.output(message);
    }

    //слушатель события нажатия на кнопку добавления в RoadInput
    @Override
    public boolean action(UndirectedEdge e){
        //исполнение метода action родителя, получение результата
        boolean result = listener.action(e);
        //если ребро добавлено успешно
        if (result){
            RoadInput ri = new RoadInput(this, list_of_items.size());
            list_of_items.add(ri);
            add(ri, c);
            //принудительное обновление дерева компонентов
            SwingUtilities.updateComponentTreeUI(this);
            return true;
        }
        else
            return false;
    }
}
