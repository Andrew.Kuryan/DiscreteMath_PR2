package sample.interfaces;

/*
интерфейс, описывающий методы для удаления элементов
 */

import sample.UndirectedEdge;

public interface Deletable{
    void delete(int position);
    void delete(UndirectedEdge edge);
}
