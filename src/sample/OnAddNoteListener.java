package sample;

/*
интерфейс, описывающий метод для реагирования на событие добавления нового ребра
 */

public interface OnAddNoteListener{
    boolean action(UndirectedEdge e);
}
