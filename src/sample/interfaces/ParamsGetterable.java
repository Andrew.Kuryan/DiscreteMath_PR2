package sample.interfaces;

/*
интерфейс, описывающий метод для возврата значения, обозначенного параметром num
 */

public interface ParamsGetterable{
    int N_NUMBER = 1;
    int M_NUMBER = 2;

    int get_parameter(int num);
}
