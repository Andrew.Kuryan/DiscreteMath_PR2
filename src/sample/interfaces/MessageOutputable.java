package sample.interfaces;

/*
интерфейс, описывающий метод для вывода на экран переданного сообщения
 */

public interface MessageOutputable{
    void output(String message);
}
