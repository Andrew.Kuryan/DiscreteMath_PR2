package sample.interfaces;

public interface Constants {

    //размеры графического окна
    int xSize = 420;
    int ySize = 600;

    //путь к каталогу с иконками для кнопок
    String icons_folder = "icons/";

    //расширение файлов с сохраненными данными
    String extension = "dmpr2";
}
